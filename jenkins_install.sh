#!/bin/bash

USERNAME=pmo-jenkins
GROUPNAME=jenkins
#need for step-try-next constractions
. ./functions

step "Checking dependenses..."
arch=`uname -m`
try type sudo wget java >/dev/null 2>&1 ||
{
	step "Installing dependenses..."
	try yum -y install sudo wget > /dev/null
	if [ arch == "x86_64"  ]; then
	{
		try yum -y install java-1.6.0-openjdk.x86_64 java-1.6.0-openjdk-devel.x86_64
	} 
	else
	{
		try yum -y install java-1.6.0-openjdk-devel java-1.6.0-openjdk
	}
	fi
	next
}
next

ver=`java -version 2>&1 | grep "java version" | awk '{print $3}' | tr -d \" | awk '{split($0, array, ".")} END{print array[2]}'`
if [[ $ver -le 5 ]]; then
{
	step "Legacy Java version! Updating...";
	try sudo yum -y update java > /dev/null
	next
}
else
	echo "[OK]"
fi

if [ ! -f /etc/yum.repos.d/jenkins.repo ]; then
{
	step "Adding Jenkins repo..." 
	try sudo wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo > /dev/null
	try sudo rpm --import http://pkg.jenkins-ci.org/redhat/jenkins-ci.org.key
	next
}
fi
step "Installing Jenkins..."
try sudo yum -y install jenkins > /dev/null
next

/bin/id -g $GROUPNAME 2>/dev/null
[ $? -ne 0 ] && 
{
	step "Creating jenkins group"
	try sudo groupadd $GROUPNAME
	next
}

/bin/id $USERNAME 2>/dev/null
[ $? -ne 0 ] &&
{
	step "Creating $USERNAME user"
	try sudo adduser $USERNAME -g $GROUPNAME
	try sudo passwd $USERNAME
	next
}

step "Getting SSH keys"
# $1,$2,$3 - links to auth keys and knowen hosts files to use with jenkins
try sudo mkdir /var/lib/jenkins/.ssh/
try sudo cp $1 /var/lib/jenkins/.ssh/id_rsa
try sudo cp $2 /var/lib/jenkins/.ssh/id_rsa.pub
try sudo cp $3 /var/lib/jenkins/.ssh/known_hosts
try sudo chown -R 700 /var/lib/jenkins/.ssh/

try sudo mkdir /home/$USERNAME/.ssh/
try cp /var/lib/jenkins/.ssh/* /home/$USERNAME/.ssh/
try sudo chown -R 700 /home/$USERNAME/.ssh/
next

step "Setting permissions"
try sudo chown -R $USERNAME /home/$USERNAME/
try sudo chown -R $USERNAME /var/lib/jenkins/ 
try sudo chown -R $USERNAME /var/log/jenkins 
next

step "Setting $USERNAME as runuser for Jenkins"
try sed -i 's/JENKINS_USER="jenkins"/JENKINS_USER="'$USERNAME'"/g' /etc/sysconfig/jenkins
next

step "Creating .gitconfig"
echo '[user]' > /var/lib/jenkins/.gitconfig
echo 'name = $USERNAME' >> /var/lib/jenkins/.gitconfig
next

#echo "Starting Jenkins service..."
sudo service jenkins start
#echo "OK"
